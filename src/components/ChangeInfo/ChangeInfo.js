import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity
  } from 'react-native';

export default class ChangeInfo extends Component {
    render(){
        return(
            <View style={{flex:1,backgroundColor:"red"}}>
                <Text>Change Info</Text>
                <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                    <Text>Goback</Text>
                </TouchableOpacity>
            </View>
        );
    }
}