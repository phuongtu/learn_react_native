import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity
  } from 'react-native';

export default class OrderHistory extends Component {
    render(){
        return(
            <View style={{flex:1,backgroundColor:"white"}}>
                <Text>Order History</Text>
                <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                    <Text>Goback</Text>
                </TouchableOpacity>
            </View>
        );
    }
}