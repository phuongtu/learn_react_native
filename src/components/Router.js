import React from 'react'
import {
    StackNavigator,
} from 'react-navigation';

import Authenticaition from './Authentication/Authentication';
import ChangeInfo from './ChangeInfo/ChangeInfo';
import Main from './Main/Main';
import OrderHistory from './OrderHistory/OrderHistory';
import DetailProduction from './Main/Shop/DetailProduction/DetailProduction';
import ListProduction from './Main/Shop/ListProduction/ListProduction';
export const HomeStack = StackNavigator({
    Screen_Home: {
        screen: Main
    },
    Screen_Authentication: {
        screen: Authenticaition
    },
    Screen_ChangeInfo: {
        screen: ChangeInfo
    },
    Screen_OrderHistory: {
        screen: OrderHistory
    },
    Screen_DetailProduction: {
        screen: DetailProduction
    },
    Screen_ListProduction: {
        screen: ListProduction
    }
}, {
        // see next line
        headerMode: 'none',
    })