import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions, StyleSheet, TextInput } from 'react-native';

const { height } = Dimensions.get('window');

export default class SingIn extends Component {
    render() {
        const { inputStyle, btnSignUp, textBtn } = style;
        return (
            <View>
                <TextInput placeholder='Enter your name' style={inputStyle} underlineColorAndroid={"transparent"} />
                <TextInput placeholder='Enter your email' style={inputStyle} underlineColorAndroid={"transparent"} />
                <TextInput placeholder='Enter your password' style={inputStyle} underlineColorAndroid={"transparent"} />
                <TextInput placeholder='Re-enter your password' style={inputStyle} underlineColorAndroid={"transparent"} />
                <TouchableOpacity style={btnSignUp}>
                    <Text style={textBtn}>SIGN UP NOW</Text>
                </TouchableOpacity >
            </View>
        );
    }
}

const style = StyleSheet.create({
    inputStyle: {
        height: 50,
        backgroundColor: '#fff',
        marginBottom: 10,
        borderRadius: 20,
        paddingLeft: 20
    },
    btnSignUp: {
        alignItems: 'center',
        borderRadius: 20,
        paddingVertical: 15,
        borderWidth: 2,
        borderColor: '#fff'
    },
    textBtn: {
        color: '#fff',
        fontSize: 15
    }
})