import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    StyleSheet
} from 'react-native';

import Header from './Header';
import SignIn from './SignIn';
import SingUp from './SignUp';

export default class Authentication extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSignIn: true
        }
    }
    openSignIn() {
        this.setState({ isSignIn: true });
    }
    openSignUp() {
        this.setState({ isSignIn: false });
    }
    render() {
        const { isSignIn } = this.state;
        const { container, controlStyle, signInStyle, signUpStyle, inactiveStyle, activeStyle } = style;
        return (
            <View style={container}>
                <Header navigation={this.props.navigation} />

                {
                    isSignIn ? <SignIn /> : <SingUp />
                }
                <View style={controlStyle}>
                    <TouchableOpacity style={signInStyle} onPress={this.openSignIn.bind(this)}>
                        <Text style={isSignIn ? activeStyle : inactiveStyle}>SIGN IN</Text>
                    </TouchableOpacity >
                    <TouchableOpacity style={signUpStyle} onPress={this.openSignUp.bind(this)}>
                        <Text style={!isSignIn ? activeStyle : inactiveStyle}>SIGN UP</Text>
                    </TouchableOpacity >
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: "#34B089",
        justifyContent: 'space-between',
    },
    controlStyle: {
        flexDirection: 'row',
    },
    signInStyle: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 15,
        backgroundColor: '#fff',
        marginRight: 1,
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20,
    },
    signUpStyle: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        paddingVertical: 15,
        marginLeft: 1,
        borderBottomRightRadius: 20,
        borderTopRightRadius: 20
    },
    inactiveStyle: {
        color: '#D7D7D7'
    },
    activeStyle: {
        color: '#34B089'
    }
})