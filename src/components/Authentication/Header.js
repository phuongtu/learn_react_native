import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, StyleSheet } from 'react-native';

import icLogo from '../../images/appIcon/ic_logo.png';
import icBack from '../../images/appIcon/back_white.png';
const { height } = Dimensions.get('window');

export default class Header extends Component {
    render() {
        const { wrapper, newRow, titleStyle, iconStyle } = style;
        return (
            <View style={wrapper}>
                <View style={newRow}>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                        <Image source={icBack} style={iconStyle} />
                    </TouchableOpacity>
                    <Text style={titleStyle}>
                        Wearing a Dress
                    </Text>
                    <Image source={icLogo} style={iconStyle} />
                </View>
            </View>
        );
    }
}

const style = StyleSheet.create({
    wrapper: { height: height / 20, backgroundColor: '#34B089', justifyContent: 'space-around' },
    newRow: { flexDirection: 'row', justifyContent: 'space-between' },
    titleStyle: { color: '#fff' },
    iconStyle: { width: 25, height: 25 }
})
