import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import Drawer from 'react-native-drawer';

import Menu from './Menu/Menu';
import Shop from './Shop/Shop';

export default class Main extends Component {
    closeControlPanel = () => {
        this.drawer.close()
    };
    openControlPanel = () => {
        this.drawer.open()
    };
    render() {
        return (
            <Drawer
                ref={(ref) => {this.drawer = ref;}}
                content={<Menu navigation={this.props.navigation}/>}
                tapToClose
                openDrawerOffset={0.5}
                >
                <Shop open={this.openControlPanel.bind(this)} navigation={this.props.navigation}/>
            </Drawer>
        );
    }
}