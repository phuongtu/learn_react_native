import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import Header from './Header'

import Home from './Home/Home';
import Search from './Search/Search';
import Cart from './Cart/Cart';
import Contact from './Contact/Contact';

import homeActive from '../../../images/appIcon/home.png';
import home from '../../../images/appIcon/home0.png';
import cartActive from '../../../images/appIcon/cart.png';
import cart from '../../../images/appIcon/cart0.png';
import searchActive from '../../../images/appIcon/search.png';
import search from '../../../images/appIcon/search0.png';
import contactActive from '../../../images/appIcon/contact.png';
import contact from '../../../images/appIcon/contact0.png';

class Shop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 'home'
        }
    }
    openMenu() {
        const { open } = this.props;
        open();
    }
    render() {
        const { iconStyle } = style;
        return (
            <View style={{ flex: 1 }}>
                <Header openMenu={this.openMenu.bind(this)} />
                <TabNavigator>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'home'}
                        title="Home"
                        renderIcon={() => <Image source={home} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={homeActive} style={iconStyle} />}
                        selectedTitleStyle={{color:'#34B089'}}
                        onPress={() => this.setState({ selectedTab: 'home' })}
                    >
                        <Home />
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'cart'}
                        title="Cart"
                        renderIcon={() => <Image source={cart} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={cartActive} style={iconStyle} />}
                        badgeText="1"
                        selectedTitleStyle={{color:'#34B089'}}
                        onPress={() => this.setState({ selectedTab: 'cart' })}

                    >
                        <Cart />
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'search'}
                        title="Search"
                        renderIcon={() => <Image source={search} style={iconStyle} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={searchActive} style={iconStyle} />}
                        selectedTitleStyle={{color:'#34B089'}}
                        onPress={() => this.setState({ selectedTab: 'search' })}
                    >
                        <Search />
                    </TabNavigator.Item>
                    <TabNavigator.Item
                        selected={this.state.selectedTab === 'contact'}
                        title="Contact"
                        renderIcon={() => <Image source={contact} style={iconStyle} />}
                        renderSelectedIcon={() => <Image source={contactActive} style={iconStyle} />}
                        selectedTitleStyle={{color:'#34B089'}}
                        onPress={() => this.setState({ selectedTab: 'contact' })}
                    >
                        <Contact />
                    </TabNavigator.Item>
                </TabNavigator>
            </View>
        );
    }
}

const style = StyleSheet.create({
    iconStyle: { width: 25, height: 25 }
})

export default Shop;