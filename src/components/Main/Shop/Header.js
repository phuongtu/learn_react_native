import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions, Image, TextInput, StyleSheet } from 'react-native';

import icMenu from '../../../images/appIcon/ic_menu.png';
import icLogo from '../../../images/appIcon/ic_logo.png';

const { height } = Dimensions.get('window');

export default class Header extends Component {
    render() {
        const { wrapper, newRow, textInput, titleStyle, iconStyle } = style;
        return (
            <View style={wrapper}>
                <View style={newRow}>
                    <TouchableOpacity onPress={this.props.openMenu}>
                        <Image source={icMenu} style={iconStyle} />
                    </TouchableOpacity>
                    <Text style={titleStyle}>
                        Wearing a Dress
                    </Text>
                    <Image source={icLogo} style={iconStyle} />
                </View>
                <TextInput
                    autoFocus={false}
                    underlineColorAndroid={"transparent"}
                    style={textInput}
                    placeholder='what do you want to buy?'
                />
            </View>
        );
    }
}

const style = StyleSheet.create({
    wrapper: { height: height / 7, backgroundColor: '#34B089', padding: 10, justifyContent: 'space-around'},
    newRow: { flexDirection: 'row', justifyContent: 'space-between' },
    textInput: { backgroundColor: '#fff', height: height / 20, paddingLeft: 10 ,paddingVertical:0},
    titleStyle: { color: '#fff' },
    iconStyle: { width: 25, height: 25 }
})
