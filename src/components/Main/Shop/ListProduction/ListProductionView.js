import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

class LsitProductionView extends Component {
    render() {
        const {
            imageProduction,
            productionContainer,
            lastRow,
            productionInfo,
            nameProduction,
            moneyProduction,
            materialProduction,
            showDetail,
            nameColorProduction
        } = style;
        return (
            <View style={productionContainer}>
                <Image source={this.props.productionImage} style={imageProduction} />
                <View style={productionInfo}>
                    <Text style={nameProduction}>Lace Sleeve Si</Text>
                    <Text style={moneyProduction}>117$</Text>
                    <Text style={materialProduction}>Material silk</Text>
                    <View style={lastRow}>
                        <Text style={nameColorProduction}>Color Red</Text>
                        <View style={{ width: 15, height: 15, margin: 5, backgroundColor: 'red', borderRadius: 7.5 }} />
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_DetailProduction') }}>
                            <Text style={showDetail}>SHOW DETAILS</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const style = StyleSheet.create({
    imageProduction: {
        height: (90 * 452) / 361,
        width: 90
    },
    productionContainer: {
        padding: 10,
        flexDirection: 'row',
        borderTopWidth: 1,
        borderColor: '#d6d7da',
        justifyContent: 'space-between',
    },
    lastRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    productionInfo: {
        justifyContent: 'space-between',
        marginLeft: 10,
        flex: 1
    },
    nameProduction: {
        fontSize: 17,
        color: '#bcbcbc'
    },
    moneyProduction: {
        color: '#b10b65'
    },
    materialProduction: {
        fontSize: 13,
    },
    showDetail: {
        color: '#b10b65',
        fontSize: 12,
    },
    nameColorProduction: {

    }
})

export default LsitProductionView;