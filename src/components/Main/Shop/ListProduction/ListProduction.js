import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView } from 'react-native';

import icBack from '../../../../images/appIcon/backList.png';
import sp1 from '../../../../images/temp/sp1.jpeg';
import sp2 from '../../../../images/temp/sp2.jpeg';
import sp3 from '../../../../images/temp/sp3.jpeg';
import sp4 from '../../../../images/temp/sp4.jpeg';
import sp5 from '../../../../images/temp/sp5.jpeg';

import ListProductionView from './ListProductionView';

class ListProduction extends Component {
    render() {
        const {
            contaiter,
            header,
            backStyle,
            title
        } = style;
        return (
            <View style={contaiter}>
                <View style={header}>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                        <Image source={icBack} style={backStyle} />
                    </TouchableOpacity>
                    <Text style={title}>Party Dress</Text>
                    <View style={{ width: 30 }} />
                </View>
                <ScrollView>
                    <ListProductionView
                        productionImage={sp1}
                        navigation={this.props.navigation}
                    />
                    <ListProductionView
                        productionImage={sp2}
                        navigation={this.props.navigation}
                    />
                    <ListProductionView
                        productionImage={sp3}
                        navigation={this.props.navigation}
                    />
                    <ListProductionView
                        productionImage={sp4}
                        navigation={this.props.navigation}
                    />
                    <ListProductionView
                        productionImage={sp5}
                        navigation={this.props.navigation}
                    />
                </ScrollView>
            </View>
        );
    }
}

const style = StyleSheet.create({
    contaiter: {
        // backgroundColor : "#DBDBDB"
        backgroundColor: "#FFF",
        margin: 10,
        padding: 10,
        flex: 1,

        // shadow ios
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        // shadow android
        elevation: 4
    },
    header: {
        height: 25,
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 10,
        marginTop: 0,
    },
    backStyle: {
        width: 30,
        height: 30
    },
    title: {
        fontSize: 15,
        color: '#B10D65'
    }
})

export default ListProduction;