import React from 'react'
import {
    StackNavigator,
} from 'react-navigation';
import HomeView from './HomeView';
import DetailProduction from '../DetailProduction/DetailProduction';
import ListProduction from '../ListProduction/ListProduction';
export const HomeStack = StackNavigator({
    Screen_Home: {
        screen: HomeView
    },
    Screen_DetailProduction: {
        screen: DetailProduction
    },
    Screen_ListProduction: {
        screen: ListProduction
    }
}, {
        // see next line
        headerMode: 'none',
    })