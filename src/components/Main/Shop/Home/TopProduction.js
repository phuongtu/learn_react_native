import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image, TouchableOpacity } from 'react-native';

const { height, width } = Dimensions.get('window');

import sp1 from '../../../../images/temp/sp1.jpeg';
import sp2 from '../../../../images/temp/sp2.jpeg';
import sp3 from '../../../../images/temp/sp3.jpeg';
import sp4 from '../../../../images/temp/sp4.jpeg';
import sp5 from '../../../../images/temp/sp5.jpeg';

class TopProduction extends Component {
    render() {
        const {
            container,
            titleContainer,
            title,
            body,
            productionImage,
            productionContainer,
            producName,
            producPrice
        } = style;
        return (
            <View style={container} >
                <View style={titleContainer} >
                    <Text style={title} >TOP PRODUCTION</Text>
                </View>
                <View style={body} >
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_DetailProduction') }}>

                        <View style={productionContainer}>
                            <Image source={sp1} style={productionImage} />
                            <Text style={producName}> PRODUCTION name</Text>
                            <Text style={producPrice}>200$</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_DetailProduction') }}>
                        <View style={productionContainer}>
                            <Image source={sp2} style={productionImage} />
                            <Text style={producName}> PRODUCTION name</Text>
                            <Text style={producPrice}>400$</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ height: 10, width }}></View>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_DetailProduction') }}>

                        <View style={productionContainer}>
                            <Image source={sp3} style={productionImage} />
                            <Text style={producName}> PRODUCTION name</Text>
                            <Text style={producPrice}>200$</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_DetailProduction') }}>
                        <View style={productionContainer}>
                            <Image source={sp4} style={productionImage} />
                            <Text style={producName}> PRODUCTION name</Text>
                            <Text style={producPrice}>400$</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ height: 10, width }}></View>
                    <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_DetailProduction') }}>
                        <View style={productionContainer}>
                            <Image source={sp5} style={productionImage} />
                            <Text style={producName}> PRODUCTION name</Text>
                            <Text style={producPrice}>400$</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const productionWidth = (width - 50) / 2;
const productionHeight = productionWidth / 0.8;
const style = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        margin: 10,
        // shadow ios
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        // shadow android
        elevation: 4
    },
    titleContainer: {
        height: 50,
        justifyContent: 'center',
        paddingLeft: 10
    },
    title: {
        fontSize: 20,
        color: '#AFAEAF'
    },
    body: {
        justifyContent: 'space-around',
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingBottom: 10
    },
    productionContainer: {
        width: productionWidth,
        // shadow ios
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        // shadow android
        borderColor: '#000000',
        elevation: 1.5

    },
    productionImage: {
        width: productionWidth,
        height: productionHeight
    },
    producName: {
        color: '#AFAEAF',
        paddingLeft: 10,
        fontFamily: 'Avenir',
        fontWeight: '500'
    },
    producPrice: {
        paddingLeft: 10,
        fontFamily: 'Avenir',
        color: '#662F90'
    }
})



export default TopProduction;