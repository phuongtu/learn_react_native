import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, Image } from 'react-native';

const { height , width} = Dimensions.get('window');

import bannerImage from '../../../../images/temp/banner.jpg';


class Collection extends Component {
    render() {
        const { wrapper, textStyle, imageStyle } = style;
        return (
            <View style={wrapper}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={textStyle}>SPRING COLLECTION</Text>

                </View>
                <View style={{flex: 4}}>
                    <Image source={bannerImage} style={imageStyle} />
                </View>
            </View>
        );
    }
}

const imageWidth = width - 40;
const imageHeight = imageWidth / 2;

const style = StyleSheet.create({
    wrapper: {
        height: height * 0.35,
        backgroundColor: "#fff",
        margin: 10,
        padding: 10,
        // shadow ios
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        // shadow android
        elevation: 4
    },
    textStyle: {
        fontSize: 20,
        color: '#AFAEAF'
    },

    imageStyle: {
        height: imageHeight,
        width: imageWidth
    }
})

export default Collection;