import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';

import Collection from './Collection';
import Category from './Category';
import TopProduction from './TopProduction';
class Home extends Component {
    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: "#DBDBDB" }}>
                <Collection />
                <Category navigation={this.props.navigation}/>
                <TopProduction navigation={this.props.navigation} />
            </ScrollView>
        );
    }
}

export default Home;