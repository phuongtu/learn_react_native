import React, { Component } from 'react';
import { View, Text, Dimensions, StyleSheet, ImageBackground, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';
const { height, width } = Dimensions.get('window');

import littleImage from '../../../../images/temp/little.jpg';
import maxiImage from '../../../../images/temp/maxi.jpg';
import partyImage from '../../../../images/temp/party.jpg';

class Category extends Component {
    render() {
        const { wrapper, textStyle, imageStyle, textImage } = style;
        return (
            <View style={wrapper}>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={textStyle}>LIST OF CATEGORY</Text>
                </View>
                <View style={{ flex: 4 }}>
                    <Swiper width={imageWidth} height={imageHeight}>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_ListProduction') }}>
                            <ImageBackground source={littleImage} style={imageStyle}>
                                <Text style={textImage}>Maxi Dress</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_ListProduction') }}>
                            <ImageBackground source={maxiImage} style={imageStyle} >
                                <Text style={textImage}>Maxi Dress</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Screen_ListProduction') }}>
                            <ImageBackground source={partyImage} style={imageStyle} >
                                <Text style={textImage}>Maxi Dress</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </Swiper>
                </View>
            </View>
        );
    }
}

const imageWidth = width - 40;
const imageHeight = imageWidth / 2;

const style = StyleSheet.create({
    wrapper: {
        height: height * 0.33,
        backgroundColor: "#fff",
        margin: 10,

        padding: 10,
        // shadow ios
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.2,
        // shadow android
        elevation: 4
    },
    textStyle: {
        fontSize: 20,
        color: '#AFAEAF'
    },
    imageStyle: {
        height: imageHeight,
        width: imageWidth,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textImage: {
        fontSize: 17,
        fontFamily: 'Avenir',
        color: '#A9A9A9'
    }
})

export default Category;