/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  View,
  Navigator
} from 'react-native';
import {HomeStack} from './HomeRouter'

export default class App extends Component {
  render() {
    return (
      <HomeStack />
    );
  }
}