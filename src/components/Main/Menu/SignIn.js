import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

class SignIn extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { btn, textBtn } = style;
        return (
            <View>
                <TouchableOpacity style={btn}
                    onPress={() => { this.props.navigation.navigate('Screen_Authentication') }}
                    // onPress={() => { this.props.onSignIn() }}
                >
                    <Text style={textBtn}> SIGN IN</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const style = StyleSheet.create({
    btn: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        paddingHorizontal: 50,
        marginBottom: 10
    },
    textBtn: {
        color: '#34B089'
    }
})

export default SignIn;