import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

import avatar from '../../../images/temp/profile.png'
import SignIn from './SignIn';
import HasSignIn from './HasSignIn'
class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogin: false
        };
        // this.onChangeState = this.onChangeState.bind(this) 
    }
    onSignIn() {
        this.setState({ isLogin: true });
    }
    onSignOut() {
        this.setState({ isLogin: false });
    }
    render() {
        const { container, imageAvatar } = style;
        return (
            <View style={container}>
                <Image source={avatar} style={imageAvatar} />
                {
                    this.state.isLogin
                        ? <HasSignIn navigation={this.props.navigation}
                            onSignOut={this.onSignOut.bind(this)}
                        />
                        : <SignIn navigation={this.props.navigation}
                            onSignIn={this.onSignIn.bind(this)}
                        />}
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#34B089',
        borderRightWidth: 3,
        borderColor: '#fff',
        alignItems: 'center'
    },
    imageAvatar: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 15,
        marginTop: 15
    }
})

export default Menu;