import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';


class Menu extends Component {

    render() {
        const { btn, textBtn, container, nameUser } = style;
        return (
            <View style={container}>
                <Text style={nameUser}>TuLP</Text>
                <View>
                    <TouchableOpacity style={btn} onPress={() => { this.props.navigation.navigate('Screen_ChangeInfo') }}>
                        <Text style={textBtn}>Change Info</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={btn} onPress={() => { this.props.navigation.navigate('Screen_OrderHistory') }}>
                        <Text style={textBtn}>Order History</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={btn} onPress={() => { this.props.onSignOut() }}>
                        <Text style={textBtn}>Sign out</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btn: {
        height: 50,
        backgroundColor: '#fff',
        justifyContent: 'center',
        borderRadius: 20,
        width: 170,
        marginBottom: 10,
        paddingLeft: 10
    },
    textBtn: {
        color: '#34B089'
    },
    nameUser: {
        color: '#fff'
    }
})

export default Menu;